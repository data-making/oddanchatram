using HTTP
using Cascadia
using Gumbo
using Dates

function fetch_prices(url)
    response = HTTP.request("GET", url)
    response_body = String(response.body)
    table_rows = eachmatch(Selector("tr"), parsehtml(response_body).root)
    rows_without_header = table_rows[2:length(table_rows)]

    prices = []

    for row in rows_without_header
        s = Selector("td")
        cells = eachmatch(s, row)
        commodity = cells[1][1][1].text
        weight = cells[2][1].text
        price = cells[3][1].text
        push!(prices, (commodity, weight, price))
    end

    prices
end

function fetch_prices_2(url)
    response = HTTP.request("GET", url)
    response_body = String(response.body)
    table_rows = eachmatch(Selector("tr"), parsehtml(response_body).root)
    rows_without_header = table_rows[2:length(table_rows)]

    prices = []

    for row in rows_without_header
        s = Selector("td")
        cells = eachmatch(s, row)
        commodity = cells[2][1][1].text
        weight = cells[3][1].text
        price = cells[4][1].text
        push!(prices, (commodity, weight, price))
    end

    prices
end

function write_to_csv(file_name, prices)
    prices_as_string = join([join(price, ",") for price in prices], "\n")
    file = open(file_name, "w")
    write(file, prices_as_string)
    close(file)
end


function get_data_urls(page_number)
    page = page_number
    page_url = "https://oddanchatramvegetablemarket.net/category/oddanchatram-vegetable-price/page/$page/"
    response = HTTP.request("GET", page_url)
    response_body = String(response.body)
    links = eachmatch(Selector(".article-content a"), parsehtml(response_body).root)
    urls = [link.attributes["href"] for link in links]
    regexp = r"comments"
    url_without_comments = filter(url -> match(regexp, url) == nothing, urls)
    url_without_comments
end
